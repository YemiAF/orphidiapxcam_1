#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QCameraViewfinder>
#include <QCameraInfo>
#include <QVideoEncoderSettings>
#include <QUrl>
#include <iostream>
#include <QFileDialog>
#include <QDebug>
#include <QColorDialog>

using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);\

    camera = nullptr;
    recorder = nullptr;

    viewfinder = new QCameraViewfinder(this);
    boxlayout = new QVBoxLayout;

    viewfinder->show();
    boxlayout->addWidget(viewfinder);



    ui->widget->setLayout(boxlayout);

    //combo box
    //ui->comboBox->addItem("Select Camera");

    //Adding a list of available cameras on the system to the comboBox
    QList<QCameraInfo> cameras = QCameraInfo::availableCameras();
    foreach (const QCameraInfo &cameraInfo, cameras)
        ui->comboBox->addItem(cameraInfo.deviceName());

    //checking if cameras are all avialable cameras are in the active state
    /*if (camera->ActiveState)
    {

        QList<QCameraInfo> cameras = QCameraInfo::availableCameras();
        for (int cam_id = 0; cam_id < cameras.count(); cam_id++)   {

            cout << cam_id << " is in the active state" << endl; }
    }

    else {

        QList<QCameraInfo> cameras = QCameraInfo::availableCameras();
        for (int cam_id = 0; cam_id < cameras.count(); cam_id++)   {

            cout << cam_id << " is not in the active state" << endl; }
    }*/



    //camera->setCaptureMode(QCamera::CaptureVideo);


    //recorder->record();

    //recorder->stop();

}

MainWindow::~MainWindow()
{
    delete ui;
}




void MainWindow::on_pushButton_clicked()
{
    if( camera == nullptr ){
        QList<QCameraInfo> cameras = QCameraInfo::availableCameras();
        for (int cam_id = 0; cam_id < cameras.count(); cam_id++)        //This for loop creates a variable cam_id, which is allows you to get the camera index for your camera
        {
            if( ui->comboBox->currentText() == cameras[cam_id].deviceName()) {
                camera = new QCamera(cameras[cam_id], this);
                QCamera::FrameRateRange(60, 60);        // sets the frame rate for the camera
                camera->setViewfinder(viewfinder);
                camera->setCaptureMode(QCamera::CaptureVideo);
                camera->start(); // to start the viewfinder - (need to make this work for an external webcam)
                ui->pushButton->setText("Stop");
                break;

            }
        }
    }
    else{
        camera->stop();
        ui->pushButton->setText("Start");
        delete camera;
        camera = nullptr;
}
}

void MainWindow::on_pushButton_2_clicked()
{

                                               ;
        //if( recorder == nullptr ){ //build on this to make it initisalise the recoder pointer in the same way as camera and then start adn stop/break loops in the same manner.



    if ( recorder == nullptr ) {



        recorder = new QMediaRecorder(camera);


        /*
         //allows the user to pick a colour after the button is clicked
         * QColor col = QColorDialog::getColor(Qt::white, this);
        if(col.isValid()) {
        QString qss = QString("background-color: %1").arg(col.name());
        ui->pushButton_2->setStyleSheet(qss);
        }*/

        qDebug()<<"Containers:"<<recorder->supportedContainers();
        qDebug()<<"Audio codecs:"<<recorder->supportedAudioCodecs();
        qDebug()<<"Video codecs:"<<recorder->supportedVideoCodecs();

        QAudioEncoderSettings audioSettings;
        audioSettings.setCodec("aac");
        audioSettings.setQuality(QMultimedia::HighQuality);

        recorder->setAudioSettings(audioSettings);

        QVideoEncoderSettings videoSettings;
        videoSettings.setCodec("avc1");
        videoSettings.setQuality(QMultimedia::HighQuality);
        videoSettings.setFrameRate(60);
        recorder->setContainerFormat("mp4");

        recorder->setVideoSettings(videoSettings);

        QString fileName = "/Users/yemi/Documents/Qt/Recordings/vid.mp4";           // select a name for your file
        //QString fileName;
        recorder->setOutputLocation(QUrl::fromLocalFile(fileName));
        recorder->record();

        qDebug()<<recorder->outputLocation().path();
        qDebug()<<recorder->outputLocation().fileName();
        qDebug()<<recorder->videoSettings().codec();
        qDebug()<<recorder->errorString();
        qDebug()<< "Recording Status:" <<recorder->status();       //added
        qDebug()<< "Recording State:" << recorder->state();        //added
        qDebug()<< "Recorrder Error Status:" << recorder->error();         //added

        /*QVideoEncoderSettings videoSettings;

        videoSettings.setCodec("video/mpeg2");
        videoSettings.setQuality(QMultimedia::HighQuality);
        videoSettings.setFrameRate(60);
        recorder->setContainerFormat("mp4");

        recorder->setVideoSettings(videoSettings);

        recorder->setOutputLocation(QUrl::fromLocalFile("test.mp4"));//QUrl::fromLocalFile("/Users/yemi/Documents/Qt/Recordings/vid.mpeg"));

        recorder->record();*/
        ui->pushButton_2->setText("Save");

        //recorder->outputLocation(QUrl("/Users/yemi/Documents/Qt/Recordings/vid.mp4"));


    }

    else {

        recorder->stop();
        //QString fileName;
        /*auto fileName = QFileDialog::getSaveFileName(this, tr("Save File"),
                                   "/Users/yemi/Documents/Qt/Recordings/vid.mp4",
                                   tr("Videos (*.mp4 *.jpg *.m4v)"));
        if (fileName.isEmpty()) {

            return;
        }*/
        /*QString file = "C:\\record.avi";
        QFile file1(fileName);
        file1.open(QIODevice::WriteOnly);
        file1.close();*/

        /*QFile file("CustomVid.p4");
        file.open(QIODevice::WriteOnly);
        QDataStream out(&file);   // we will serialize the data into the file
        out << QMediaRecorder(*recorder);   // serialize a string*/
        
        




        ui->pushButton_2->setText("Record");
        delete recorder;
        recorder = nullptr;
    }



}








    /*
     *
     *
     *THIS IS AN OLDER VERSION OF THE CODE**********
     *
     * if (camera->state() == QCamera::ActiveState) {

       camera->stop();
       return;
   }

else {
    QList<QCameraInfo> cameras = QCameraInfo::availableCameras();
    for (int cam_id = 0; cam_id < cameras.count(); cam_id++)        //This for loop creates a variable cam_id, which is allows you to get the camera index for your camera
    {
        if( ui->comboBox->currentText() == cameras[cam_id].deviceName()) {
            if( camera != nullptr ){
                //camera->stop();
                delete camera;

                camera = new QCamera(cameras[cam_id], this);
                camera->setViewfinder(viewfinder);
            }
            camera->start(); // to start the viewfinder - (need to make this work for an external webcam)
        }
        else {
            camera->stop();
        }

    return;
    }
    }*/


  /*  if(camera->ActiveState) {

        ui->pushButton->setText("Stop");
    }*/ //need to work on this part so that the camera stops streaming when the button is clicked again



